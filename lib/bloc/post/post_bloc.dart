import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_demo/bloc/post/post_event.dart';
import 'package:flutter_demo/bloc/post/post_state.dart';
import 'package:flutter_demo/services/api_repository.dart';

class PostBloc extends Bloc<PostEvent, PostState> {
  PostBloc() : super(const PostStateInit()) {
    // Get post event
    on<GetPostEvent>(_handleGetPost);
  }

  // Handle get post
  // @param event: GetPostEvent
  // @param emit: de emit state
  _handleGetPost(GetPostEvent event, Emitter emit) async {
    emit(const GetPostLoading());
    await Future.delayed(Duration(seconds: 2));
    try {
      final result = await ApiRepository.instance.getPosts();
      emit(GetPostSuccess(posts: result));
    } catch (e) {
      emit(GetPostError(errMsg: e.toString()));
    }
  }
}
