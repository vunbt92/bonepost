import 'package:equatable/equatable.dart';
import 'package:flutter_demo/model/post_model.dart';

abstract class PostState extends Equatable {
  const PostState();

  @override
  List<Object?> get props => [];
}

// Call when init
class PostStateInit extends PostState {
  const PostStateInit();
}

// Call when loading
class GetPostLoading extends PostState {
  const GetPostLoading();
}

// Call when success
class GetPostSuccess extends PostState {
  // List post
  final List<PostModel> posts;

  const GetPostSuccess({required this.posts});

  @override
  List<Object?> get props => [posts];
}

// Call when error
class GetPostError extends PostState {
  // List post
  final String errMsg;

  const GetPostError({required this.errMsg});

  @override
  List<Object?> get props => [errMsg];
}
