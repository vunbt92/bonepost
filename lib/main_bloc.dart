import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_demo/bloc/post/post_bloc.dart';

class MainBloc {
  static List<BlocProvider> allBlocs() => [
        // Post
        BlocProvider<PostBloc>(
            lazy: true, create: (BuildContext context) => PostBloc()),
      ];
}
