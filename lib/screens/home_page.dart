import 'package:flutter/material.dart';
import 'package:flutter_demo/bloc/post/post_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_demo/bloc/post/post_event.dart';
import 'package:flutter_demo/bloc/post/post_state.dart';
import 'package:flutter_demo/model/post_model.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<PostModel> _posts = [];

  @override
  void initState() {
    // Call posts
    context.read<PostBloc>().add(const GetPostEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<PostBloc, PostState>(
        builder: (context, state) => _buildUI(context, state),
        listener: (context, state) {
          if (state is GetPostLoading) {
            print('Loading....');
          } else if (state is GetPostSuccess) {
            print('Success... ${state.posts.length}');
            _posts = state.posts;
          }
        });
  }

  // Build main UI
  Widget _buildUI(BuildContext context, PostState state) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: _buildList(context, state),
      ),
    );
  }

  Widget _buildList(BuildContext context, PostState state) {
    if (state is GetPostLoading) {
      return const CircularProgressIndicator();
    }
    return ListView.builder(
        itemCount: _posts.length,
        itemBuilder: (context, index) {
          final item = _posts[index];
          return Container(
            padding: const EdgeInsets.all(16),
            child: Text(
              item.body ?? '',
              style: Theme.of(context).textTheme.bodyText1,
            ),
          );
        });
  }
}
