import 'package:flutter_demo/model/post_model.dart';

abstract class ApiInterface {
  Future<List<PostModel>> getPosts();
}
