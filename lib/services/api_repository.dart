import 'dart:convert';

import 'package:flutter_demo/model/post_model.dart';
import 'package:flutter_demo/services/api_constant.dart';
import 'package:flutter_demo/services/api_interface.dart';
import 'package:http/http.dart' as http;

class ApiRepository implements ApiInterface {
  // Instance
  static late ApiRepository _instance;

  ApiRepository._internal();

  // Get instance
  static ApiRepository get instance {
    _instance = ApiRepository._internal();
    return _instance;
  }

  @override
  Future<List<PostModel>> getPosts() async {
    List<PostModel> list = [];
    final result =
        await http.get(Uri.parse(ApiConstant.baseURL + ApiConstant.post));
    if (result != null && result.statusCode == 200) {
      final response = jsonDecode(result.body);
      response.forEach((e) {
        list.add(PostModel.fromJson(e));
      });
    }
    return list;
  }
}
