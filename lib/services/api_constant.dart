class ApiConstant {
  ApiConstant._();

  static const baseURL = "https://jsonplaceholder.typicode.com/";
  static const post = "posts";
}
